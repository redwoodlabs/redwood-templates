<div class="form-control">
  <div class="s3direct" style="float:none; overflow: hidden;" data-policy-url="{{ policy_url }}" data-signing-url="{{ signing_url }}">
    <div><img src="{{ file_url }}" height="40"></div>
    <a class="file-link" target="_blank" href="{{ file_url }}"><i class="fa fa-file-image-o"></i> {{ file_name }}</a>
    <a class="file-remove btn btn-sm btn-rounded btn-base-3" href="#remove">Remove</a>
    <input class="csrf-cookie-name" type="hidden" value="{{ csrf_cookie_name }}">
    <input class="file-url" type="hidden" value="{{ file_url }}" id="{{ element_id }}" name="{{ name }}" />
    <input class="file-dest" type="hidden" value="{{ dest }}">
    <input class="file-input" type="file"  style="{{ style }}"/>
    <div class="progress progress-striped active">
      <div class="bar"></div>
      <a href="#cancel" class="cancel-button">&times;</a>
    </div>
  </div>
</div>

