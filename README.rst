=====
Redwood Templates
=====

This is the template overrides package used for Redwood Labs projects.

Quick start
-----------

1. Add "redwood-templates" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'redwood-templates',
    ]
